**INDICE PARZIALE ESEMPI JPF**

L'indice totale si può trovare nel repository triangles.

Clonare con git clone --recurse-submodules.
Settare JDK11 e su IDEA lanciare comando "Invalidate Caches/Restart".

| Assignment  | Argomento Lezione | Task Svolti |
| ---------- | --------- | ------------------- |
| [Assignment17](https://gitlab.com/paolino625/esempijpf/-/tree/calcagni_paolo_assignment17)  | JPF. | Reso il programma thread-safe. Testata la correttezza con JPF. |
| [Assignment17bis](https://gitlab.com/paolino625/esempijpf/-/tree/calcagni_paolo_assignment17bis)  | JPF. | Aggiunto metodo dec a classe Counter. Tolto sollevamento eccezione manuale per race condition e attivato listener apposito. |
