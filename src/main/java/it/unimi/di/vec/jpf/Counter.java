package it.unimi.di.vec.jpf;

public class Counter implements Runnable {

  private long contatore = 0;

  public Counter(long start) {

    this.contatore = start;
  }

  public synchronized void inc() {

    this.contatore += 1;
  }

  public synchronized void dec() {
    this.contatore -= 1;
  }

  public long get() {

    return this.contatore;
  }

  @Override
  public void run() {
    inc();
    dec();
  }
}
