package it.unimi.di.vec.jpf;

import java.util.Random;

public class Rand {
  public static void main(String[] args) {
    Random random = new Random(42);

    int a = random.nextInt(2);
    System.out.println("a=" + a);

    int b = random.nextInt(3);
    System.out.println("  b=" + b);

    // e se b + a == 2???
    int c = a / (b + a - 2);
    System.out.println("    c=" + c);
  }
}
