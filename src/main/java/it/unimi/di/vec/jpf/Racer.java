package it.unimi.di.vec.jpf;

public class Racer implements Runnable {

  int num = 100;

  public static void main(String[] args) {
    Racer r = new Racer();
    Thread t = new Thread(r);
    t.start();

    sleepForAWhile(1000);
    System.out.println(100 / r.num);
  }

  @Override
  public void run() {
    sleepForAWhile(1001);
    num = 0;
  }

  static void sleepForAWhile(long ms) {
    try {
      Thread.sleep(ms);
    } catch (InterruptedException e) {
      System.err.println("Interrupted!");
    }
  }
}
