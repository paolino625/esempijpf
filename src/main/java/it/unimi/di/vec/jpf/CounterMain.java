package it.unimi.di.vec.jpf;

import java.util.ArrayList;
import java.util.List;

public class CounterMain {
  public static void main(String[] args) throws Exception {
    Counter counter = new Counter(0);
    int numeroThread = 10;

    List<Thread> arrayThread = new ArrayList<>();

    // Creo thread e li aggiungo all'array
    for (int i = 0; i < numeroThread; i++) {
      Thread t = new Thread(counter);
      arrayThread.add(t);
    }

    // Eseguo thread
    for (Thread t : arrayThread) {
      t.start();
    }

    // Aspetto che i thread finiscano
    for (Thread t : arrayThread) {
      t.join();
    }

    System.out.println("Contatore: " + counter.get());
  }
}
